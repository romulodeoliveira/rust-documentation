<h1 align="center">🦀 Rust Documentation 🦀</h1>

<br>
Repositório destinado a registrar meus estudos com a linguagem Rust seguindo a documentação oficial.

🌐 | https://rust-br.github.io/rust-book-pt-br/title-page.html

<br>
Comando muito utilizado por para criação dos projetos Cargo sem o Git (mantido aqui para consulta particular):

<code>cargo new <nome_do_projeto> --vcs none</code>
