/*
cada valor em rust é um certo tipo de dado. 

rust é uma linguagem de tipagem estática , o que significa que deve conhecer os tipos de todas as variáveis ​​em tempo de compilação.
*/

fn main() {
    println!("TIPOS ESCALARES:\n");

    println!("Maior valor de i8: {}", std::i8::MAX);
    println!("Menor valor de i8: {}", std::i8::MIN);

    println!("");

    println!("Maior valor de u8: {}", std::u8::MAX);
    println!("Menor valor de u8: {}", std::u8::MIN);

    println!("");

    println!("Maior valor de i16: {}", std::i16::MAX);
    println!("Menor valor de i16: {}", std::i16::MIN);

    println!("");

    println!("Maior valor de u16: {}", std::u16::MAX);
    println!("Menor valor de u16: {}", std::u16::MIN);

    println!("");

    println!("Maior valor de i32: {}", std::i32::MAX);
    println!("Menor valor de i32: {}", std::i32::MIN);

    println!("");

    println!("Maior valor de u32: {}", std::u32::MAX);
    println!("Menor valor de u32: {}", std::u32::MIN);

    println!("");

    println!("Maior valor de i64: {}", std::i64::MAX);
    println!("Menor valor de i64: {}", std::i64::MIN);

    println!("");

    println!("Maior valor de u64: {}", std::u64::MAX);
    println!("Menor valor de u64: {}", std::u64::MIN);

    println!("");

    println!("Maior valor de i128: {}", std::i128::MAX);
    println!("Menor valor de i128: {}", std::i128::MIN);

    println!("");

    println!("Maior valor de u128: {}", std::u128::MAX);
    println!("Menor valor de u128: {}", std::u128::MIN);
}


/*
Tipos escalares:

 Rust tem quatro tipos escalares principais: inteiros, float, booleanos e strings
*/
