/*
Por padrão, variáveis são imutáveis, mas podemos mudar isso da seguinte forma:
*/ 

fn main() {
    let mut str = "Romulo";
    println!("My name is: {str}");
    str = "Jose";
    println!("My name is: {str}");
}
