extern crate rand;

use std::io;
use std::cmp::Ordering;
use rand::Rng;

/*
Para obter a entrada do usuário, e então imprimir o resultado como saída, precisaremos trazer ao escopo a biblioteca io (de entrada/saída).
A biblioteca io provém da biblioteca padrão (chamada de std):
*/

fn main() {
    println!("Adivinhe o número!");

    // houve uma pequena mudança. Documentação: https://docs.rs/rand/latest/rand/
    let mut rng = rand::thread_rng();
    let numero_secreto = rng.gen_range(1..=100);

    println!("O número secreto é: {}", numero_secreto);


    let confirmacao = "Y";

    while confirmacao == "Y" {

        println!("\nDeseja continuar?\nDigite: 'Y' para sim ou 'N' para não:");

        let mut confirmacao = String::new();

        io::stdin().read_line(&mut confirmacao)
            .expect("Falha ao ler a entrada.");

        confirmacao = confirmacao.trim().to_string();

        if confirmacao == "Y" {

            println!("\nDigite o seu palpite.");

            let mut palpite = String::new();

            io::stdin().read_line(&mut palpite)
                .expect("Falha ao ler a entrada.");
            
            /*
            "read_line" é uma função pertencente a bibioleta io.
            
            Nesse caso, ela recebe um argumento: "&mut palpite" - o símbolo & indica que o argumento é uma referência.

            O método "expect" serve para tratamento de erro.
            */


            /*
            Tratando entradas inválidas:
            */

            let palpite: u32 = match palpite.trim().parse() {
                Ok(num) => num,
                Err(_) => continue,
            };

            /*
            Normalmente, quando queremos converter um valor de um tipo em outro, podemos "sombrear" uma variável antiga com uma nova variável. Foi o que aconteceu com "palpite".
            */

            /*
            O método parse em strings converte uma string para algum tipo de número.
            */

            println!("Você disse: {}", palpite); // String formatada... parece Java, C e Python. Nada de novo.

            match palpite.cmp(&numero_secreto) {
                Ordering::Less => println!("Muito baixo!"),
                Ordering::Greater => println!("Muito alto!"),
                Ordering::Equal => {
                    println!("Você acertou!");
                    break;
                }
            }

        }

        else {
            break;
        }

    }
}

/*
Crates.io é onde as pessoas do ecossistema Rust postam seus projetos open source para que os outros possam usar.

https://crates.io/
*/