fn main() {
    println!("Hello, world!");
}

/*
Para compilar o arquivo podemos executar o seguinte comando no terminal:

$ rustc main.rs
*/

/*
Para executar o arquivo:

$ ./main
*/