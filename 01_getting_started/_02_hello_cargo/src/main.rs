fn main() {
    println!("Hello, world!");
}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
/*
Cargo é o gestor de sistemas e pacotes da linguagem Rust.
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
/*
Para criar um projeto Cargo sem o Git:

$ cargo new <nome_do_projeto> --vcs none
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
/*
Para compilar:

$ cargo build

Para executar:

$ cargo run

Para verificar se o código pode ser compilado:

$ cargo check
*/
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
